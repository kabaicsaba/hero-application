package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.model.Hero;
import com.example.demo.repository.HeroRepository;



@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
public class HeroRepositoryITest {
	@Autowired
    private HeroRepository heroRepository;
	
	@Test
    public void testRetrieveHeros() {

        Iterable<Hero> allHeros = heroRepository.findAll();

        long HeroId = allHeros.iterator().next().getId();

        Hero hero = heroRepository.findOne(HeroId);

        assertThat(hero).isNotNull();


        assertThat(hero.getFullName()).isEqualTo("Thor Odinson");
        assertThat(hero.getFullName()).startsWith("Thor").endsWith("Odinson");
    }
	
	@Test
	public void test1() {
		List<Hero> hero=heroRepository.findByFullName("Thor Odinson");

       
        assertThat(hero).isNotNull();


        assertThat(hero.get(0).getFullName()).isEqualTo("Thor Odinson");
        assertThat(hero.get(0).getFullName()).startsWith("Thor").endsWith("Odinson");
	}
	
	@Test
	public void test2() {
		List<Hero> hero=heroRepository.findByFullNameLike("%e%");

       
        assertThat(hero).isNotNull();
        assertThat(hero).hasSize(3);

        
	}
	
	
	/*
	   List<Hero> findByFullName(String fullName);

	    List<Hero> findByFullNameLike(String fullName);

	    List<Hero> findTop3ByFullNameLikeOrderByFullName(String fullName);*/
}
