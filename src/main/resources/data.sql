INSERT INTO type (id, spec,species)
VALUES
('1', 'god of thunder','WARRIOR'),
('2', 'smash','WARRIOR'),
('3', 'flying','GENIUS'),
('4', 'wall climbing','GENIUS'),
('5', 'teleportation','WIZARD'),
('6', 'wizardry','WARRIOR'),
('7','wonder','WARRIOR');

INSERT INTO hero ( id , full_name,  age ,height ,weight,origin,sex,description,types_id) 
VALUES 
('1', 'Thor Odinson', '2000', '198','290','Asgard','MALE','As the son of Odin and Gaea, Thors strength, endurance and resistance to injury are greater than the vast majority of his superhuman race. He is extremely long-lived (though not completely immune to aging), immune to conventional disease and highly resistant to injury.','1'),
('2', 'Hulk', '40', '244','635','Earth','MALE','The Hulk possesses an incredible level of superhuman physical ability. His capacity for physical strength is potentially limitless due to the fact that the Hulks strength increases proportionally with his level of great emotional stress, anger in particular. ','2'),
('3', 'Iron Man','40', '198','192','Earth','MALE','Tony has a genius level intellect that allows him to invent a wide range of sophisticated devices, specializing in advanced weapons and armor. He possesses a keen business mind.','3'),
('4', 'Spider Man', '16', '155','75','Earth','MALE','Peter can cling to most surfaces, has superhuman strength (able to lift 10 tons optimally) and is roughly 15 times more agile than a regular human. The combination of his acrobatic leaps and web-slinging enables him to travel rapidly from place to place.','4'),
('5', 'Doctor Strange', '40', '188','82','Earth','MALE','Doctor Strange is one of the most powerful sorcerers in existence. Like most sorcerers, he draws his power from three primary sources: the invocation of powerful mystic entities or objects, the manipulation of the universes ambient magical energy, and his own psychic resources. ','5'),
('6', 'Iron Fist', '26','180','80','Earth','MALE','Through concentration, Iron Fist can harness his spiritual energy, or chi, to augment his physical and mental capabilities to peak human levels. By focusing his chi into his hand, he can tap the superhuman energy of Shou-Lao and temporarily render his fist superhumanly powerful. ','6'),
('7','Captain Marvel','25','180','75','Earth','FEMALE','Ms. Marvels current powers include flight, enhanced strength, durability and the ability to shoot concussive energy bursts from her hands.','7');

INSERT INTO ability (id, Ab_Name,level,hero_id)
VALUES
('1', 'super strength','MASTER','1'),
('2', 'super strength','MASTER','2'),
('3', 'high IQ','MASTER','3'),
('4', 'web shot','LOW','4'),
('5', 'wall climbing','MEDIUM','4'),
('6', 'flying','HIGH','1'),
('7', 'flying','HIGH','3'),
('8', 'teleportation','MASTER','5'),
('9', 'wizardry','HIGH','6'),
('10', 'energy shot','MEDIUM','7');

