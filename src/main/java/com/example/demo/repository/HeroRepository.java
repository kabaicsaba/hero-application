package com.example.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Hero;
import com.example.demo.model.Type.Species;


public interface HeroRepository extends CrudRepository<Hero, Long> {

    Page<Hero> findAllByOrderByFullName(Pageable pageable);

    Page<Hero> findDistinctByFullNameIgnoreCaseLikeOrderByFullName(
            String fullName, Pageable page);
    Page<Hero> findDistinctTypesSpeciesIgnoreCaseLikeOrderByFullName(
    		Species warrior, Pageable page);
   /* Page<Hero> findByTypesSpecises_IgnoreCase(
    		Species warrior, Pageable page);*/
    
  
    
    @EntityGraph(value = "ability", type = EntityGraphType.LOAD)
    Hero findOneById(Long id);

    List<Hero> findByFullName(String fullName);

    List<Hero> findByFullNameLike(String fullName);
    Page<Hero> findByFullNameLike(
            String fullName, String name, Pageable page);

    List<Hero> findTop3ByFullNameLikeOrderByFullName(String fullName);

    

   
    

    

}
