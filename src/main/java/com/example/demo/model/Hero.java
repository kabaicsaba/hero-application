package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(indexes = { @Index(name = "HERO_FULLNAME_IDX", columnList = "fullname") })
@NamedEntityGraphs({@NamedEntityGraph(name = "ability", attributeNodes = @NamedAttributeNode("abilities") ) })
public class Hero {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Size(min = 3, max = 80)
    @NotNull
    private String fullName;
    
    @NotNull
    private int age;
    @NotNull
    private int height;
    @NotNull
    private int weight;
    @NotNull
    private String origin;
    @OneToMany(cascade = CascadeType.ALL) // lazy fetching
    @JoinColumn(name = "HERO_ID")
    @Valid
    @NotNull
    private List<Ability> abilities = new ArrayList<>();
    public enum sex {MALE , FEMALE}
	@NotNull
	@Enumerated(EnumType.STRING)
	private sex sex;
	@NotNull
	private String description;
	@OneToOne(cascade = { CascadeType.ALL }) // eager fetching
    @JoinColumn
    @Valid
    @NotNull
    private Type types;
	
	/*@Transient
	private MultipartFile image;
	*/
    public Hero() {}

    public Hero(String fullName,int age,int height,int weight,String origin,sex sex,String description) {
        this.fullName = fullName;
        this.age=age;
        this.height=height;
        this.weight=weight;
        this.origin=origin;
        this.sex=sex;
        this.description=description;
    }
	
    public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public sex getSex() {
		return sex;
	}

	public void setSex(sex sex) {
		this.sex = sex;
	}

	

	/*public MultipartFile getImage() {
		return image;
	}

	public void setUserImage(MultipartFile image) {
		this.image = image;
	}*/

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
		
    public Type getTypes() {
		return types;
	}

	public void setTypes(Type types) {
		this.types = types;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "Hero [id=" + id + ", fullName=" + fullName + "]";
    }

}
