package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Type {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    public enum Species { WARRIOR , DRUID , GENIUS , WIZARD }
    @NotNull
    @Enumerated(EnumType.STRING)
    private Species species;
    @NotNull
    @Size(max = 30)
    private String spec;

    public Type() {}

    public Type(Species species, String spec) {
        this.species=species;
        this.spec=spec;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public Species getSpecies() {
		return species;
	}

	public void setSpecies(Species species) {
		this.species = species;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}
	@Override
    public String toString() {
        return "Type [type=" + species + ", spec=" + spec + "]";
    }

}
