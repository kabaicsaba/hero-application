package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Ability {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    public enum Level { MASTER , HIGH , MEDIUM, LOW }
    @NotNull
    @Enumerated(EnumType.STRING)
    private Level level;
    @NotNull
    @Size(max = 30)
    private String AbName;

    public Ability() {}

    public Ability(Level level, String AbName) {
        this.level = level;
        this.AbName = AbName;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getAbName() {
        return AbName;
    }

    public void setAbName(String AbName) {
        this.AbName = AbName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Ability [level=" + level + ", AbName=" + AbName + "]";
    }

}
