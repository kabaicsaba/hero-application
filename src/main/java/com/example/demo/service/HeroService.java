package com.example.demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.model.Hero;
import com.example.demo.model.Type.Species;


public interface HeroService {


    Page<Hero> searchByName(String searchTerm, Pageable page);
  

    Hero getHeroDetails(long employeeId);

}
