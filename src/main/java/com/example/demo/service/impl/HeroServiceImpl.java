package com.example.demo.service.impl;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Hero;
import com.example.demo.model.Type.Species;
import com.example.demo.repository.HeroRepository;
import com.example.demo.service.HeroService;


@Service
public class HeroServiceImpl implements HeroService {

    @Autowired
    private HeroRepository heroRepository;

    @Transactional(readOnly = true)
    public Hero getHeroDetails(long heroId) {
        Hero result = heroRepository.findOneById(heroId);

        return result;
    }

    public Page<Hero> searchByName(String searchTerm, Pageable page) {

        Page<Hero> heroResult = null;

        if (searchTerm == null || searchTerm.length() == 0) {

            heroResult = heroRepository.findAllByOrderByFullName(page);

        } else {
            searchTerm = searchTerm.trim();

            searchTerm = MessageFormat.format("%{0}%", searchTerm);

            heroResult = heroRepository
                    .findDistinctByFullNameIgnoreCaseLikeOrderByFullName(
                            searchTerm,  page);
        }

        return heroResult;
    }

	
    


}
