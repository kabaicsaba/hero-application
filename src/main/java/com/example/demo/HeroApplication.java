package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeroApplication {
/*Sajnos az utóbbi időben személyes okok miatt nem volt lehetőségem 
a Bootcamp final project-jére időt szánni, így csupán az azt megelőző gyakorlást 
tudom bemutatni, ami a példaként feltöltött hello-jpa project alapján készült a jpa funkcióinak
 megismerése céljából.
	*/
	public static void main(String[] args) {
		SpringApplication.run(HeroApplication.class, args);
	}
}
