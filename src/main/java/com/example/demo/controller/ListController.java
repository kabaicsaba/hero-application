package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.service.HeroService;
import com.example.demo.model.Type;



@Controller
public class ListController {

    @Autowired
    private HeroService heroService;

 
    @RequestMapping(value = "/list.html")
    public String displayHeroList(Model model, @RequestParam(required = false) String searchTerm,  Pageable page) {

        if (page.getPageSize() < 1 || page.getPageSize() > 10) {
            page = new PageRequest(0, 6);
        }

        model.addAttribute("herosPage", heroService.searchByName(searchTerm, page));
        return "list";
    }

}
