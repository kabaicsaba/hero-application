package com.example.demo.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.demo.controller.form.HeroForm;
import com.example.demo.model.Ability;
import com.example.demo.model.Hero;
import com.example.demo.repository.HeroRepository;
import com.example.demo.service.HeroService;



@Controller
@RequestMapping("/hero.html")
public class HeroController {

    private static final String HERO_FORM = "heroForm";
    private static final String VIEW_HERO = "hero";
    @Autowired
    private HeroRepository heroRepository;
    @Autowired
    private HeroService heroService;
    @Resource
    private ConversionService conversionService;

    
    @RequestMapping(method = RequestMethod.GET)
    public String displayHero(Model model, @RequestParam(required = false) Long heroId) {

        Hero hero = heroId != null ? heroService.getHeroDetails(heroId) : new Hero();

        HeroForm form = conversionService.convert(hero, HeroForm.class);

        model.addAttribute(HERO_FORM, form);
        return VIEW_HERO;
    }

  
    @RequestMapping(method = RequestMethod.POST)
    public String createOrUpdateHero(@Valid @ModelAttribute(HERO_FORM) HeroForm heroForm,
            BindingResult bindingResult) {

        if (heroForm.getAction() != null) {

            switch (heroForm.getAction()) {

            case SAVE:
                if (bindingResult.hasErrors()) {
                    bindingResult.reject("hero.error.userInput");
                    return VIEW_HERO;
                }

                Hero hero = conversionService.convert(heroForm, Hero.class);

                try {
                    hero = heroRepository.save(hero);
                } catch (ObjectOptimisticLockingFailureException lockingException) {
                    bindingResult.reject("hero.error.concurrentModification");
                    return VIEW_HERO;
                }
                break;

            case DELETE:
                heroRepository.delete(heroForm.getHero());
                break;

            case BACK:
                break;

            case ADD_ABILITY:
                heroForm.getHero().getAbilities().add(new Ability());
                return VIEW_HERO;
            }
        } else {
        	deleteEntry(heroForm.getHero().getAbilities(), heroForm.getDeleteAbilityIndex());
            
            heroForm.setDeleteAbilityIndex(null);

            return VIEW_HERO;
        }

        return "redirect:/";

    }

   
    private <T> void deleteEntry(List<T> list, Integer indexOfElementToDelete) {
        if (indexOfElementToDelete != null && indexOfElementToDelete <= list.size()) {
            list.remove((int) indexOfElementToDelete);
        }
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleNotExistingHero(EmptyResultDataAccessException e) {
        return "error/hero-does-not-exist";
    }

 

}
