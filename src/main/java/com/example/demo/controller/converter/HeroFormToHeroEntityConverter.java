package com.example.demo.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.demo.controller.form.HeroForm;
import com.example.demo.model.Hero;


@Component
public class HeroFormToHeroEntityConverter implements Converter<HeroForm, Hero> {

    @Override
    public Hero convert(HeroForm source) {
        return source.getHero();
    }

}
