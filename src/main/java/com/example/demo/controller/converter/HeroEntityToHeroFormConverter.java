package com.example.demo.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.demo.controller.form.HeroForm;
import com.example.demo.model.Hero;


@Component
public class HeroEntityToHeroFormConverter implements Converter<Hero, HeroForm> {

    @Override
    public HeroForm convert(Hero hero) {

        HeroForm form = new HeroForm();
        form.setHero(hero);

        return form;
    }

}
