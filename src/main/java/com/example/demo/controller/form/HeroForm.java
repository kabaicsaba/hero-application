package com.example.demo.controller.form;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.example.demo.model.Hero;



public class HeroForm {
    @Valid
    @NotNull
    private Hero hero;
    @NotNull
    private Action action;
    public static enum Action {SAVE, BACK, DELETE, ADD_ABILITY}
    @Min(0)
    private Integer deleteAbilityIndex;

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Integer getDeleteAbilityIndex() {
        return deleteAbilityIndex;
    }

    public void setDeleteAbilityIndex(Integer deleteAbilityIndex) {
        this.deleteAbilityIndex = deleteAbilityIndex;
    }

}
